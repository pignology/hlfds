/*
** HamLog Field Day Server
** hlfds.c
** Copyright (C) 2017  Nick Garner / N3WG, Pignology
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <fcntl.h>
#include <termios.h>
#include <asm-generic/ioctls.h>
#include <time.h>
#include <sqlite3.h>

#define PORT "7373"   // port we're listening on

int debug = 1;

sqlite3 *db;

fd_set master;    // master file descriptor list
fd_set read_fds;  // temp file descriptor list for select()
int fdmax;        // maximum file descriptor number

int listener;     // listening socket descriptor
int newfd;        // newly accept()ed socket descriptor
struct sockaddr_storage remoteaddr; // client address
socklen_t addrlen;
char remoteIP[INET6_ADDRSTRLEN];

int yes=1;        // for setsockopt() SO_REUSEADDR, below
int i, j, rv;

struct addrinfo hints, *ai, *p;

char buf[65536];    // buffer for tcp client data
int nbytes;


// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}


int main(int argc, char **argv)
{

    printf("Starting HL FDS Server\n\n");

    /* get command line options */
    int c;
    opterr = 0;

    while ((c = getopt (argc, argv, "d")) != -1)
    {
      switch (c)
      {
        case 'd':
          debug = 1;
          break;
        case '?':
          if (optopt == 'c')
            fprintf (stderr, "Option -%c requires an argument.\n", optopt);
          else if (isprint (optopt))
            fprintf (stderr, "Unknown option `-%c'.\n", optopt);
          else
            fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
            return 1;
        default:
            abort ();
      }
    }

    if (debug)
    {
      printf("Debug enabled.\n");
    }

    // Open DB and create table if needed
    int retval;
    retval = sqlite3_open("/opt/hlfds/hlfds.sqlite3", &db);
    if (retval)
    {
      printf("Unable to open database!\n");
      return -1;
    }
    printf("Database opened.\n");

    //create table
    char *dberr = NULL;
    retval = sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS logentries (luuid TEXT PRIMARY KEY, date INTEGER, device TEXT, band TEXT, mode TEXT, callsign TEXT, class TEXT, section  TEXT, operator TEXT, comment TEXT)", 0, 0, &dberr);
    if (dberr != NULL)
    {
        printf("DB error: %s", dberr);
        exit(1);
    }

    //set up networking
    FD_ZERO(&master);    // clear the master and temp sets
    FD_ZERO(&read_fds);

    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(NULL, PORT, &hints, &ai)) != 0) {
        if (debug) fprintf(stderr, "hlfds: %s\n", gai_strerror(rv));
        exit(2);
    }

    for(p = ai; p != NULL; p = p->ai_next) {
        listener = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (listener < 0) {
            continue;
        }

        setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

        if (bind(listener, p->ai_addr, p->ai_addrlen) < 0) {
            close(listener);
            continue;
        }

        break;
    }

    // if we got here, it means we didn't get bound
    if (p == NULL) {
        if (debug) fprintf(stderr, "hlfds: failed to bind\n");
        exit(3);
    }

    freeaddrinfo(ai);

    // listen
    if (listen(listener, 10) == -1) {
        perror("listen");
        exit(4);
    }

    // add the listener to the master set
    FD_SET(listener, &master);

    // keep track of the biggest file descriptor
    fdmax = listener; // so far, it's this one

    // main loop
    for(;;) {
        //select timeout, recreate each time
        struct timeval timeout;
        timeout.tv_sec = 0;
        timeout.tv_usec = 550000;
        //if (debug) printf("select timeout: %d.%d", timeout.tv_sec, timeout.tv_usec);

        read_fds = master; // copy it
        if (select(fdmax+1, &read_fds, NULL, NULL, &timeout) == -1) {
            perror("select");
            exit(4);
        }

        // run through the existing connections looking for data to read
        for(i = 0; i <= fdmax; i++) 
        {
            if (FD_ISSET(i, &read_fds)) 
            {
                if (i == listener) 
                {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
                    newfd = accept(listener, (struct sockaddr *)&remoteaddr,  &addrlen);

                    if (newfd == -1) 
                    {
                        perror("accept");
                    } 
                    else 
                    {
                        FD_SET(newfd, &master); 
                        if (newfd > fdmax) 
                        {    
                            fdmax = newfd;
                        }
                        if (debug) 
                        {
                          printf("hlfds: new connection from %s on  socket %d\n",
                            inet_ntop(remoteaddr.ss_family, 
                                get_in_addr((struct sockaddr*)&remoteaddr),  
                                remoteIP, 
                                INET6_ADDRSTRLEN),
                            newfd);
                        }
                    }
                }
                else 
                {
                    // handle data from a client
                    if ((nbytes = recv(i, buf, sizeof buf, 0)) <= 0) 
                    {
                        // got error or connection closed by client
                        if (nbytes == 0) 
                        {
                            // connection closed
                            if (debug) 
                            {   
                                printf("hlfds: host %s, socket %d, hung up\n", inet_ntop(remoteaddr.ss_family,get_in_addr((struct sockaddr*)&remoteaddr), remoteIP, INET6_ADDRSTRLEN), i);
                            }
                        } 
                        else 
                        {
                            perror("recv");
                        }
                        close(i);
                        FD_CLR(i, &master); // remove from master set
                    } 
                    else 
                    {
                        // we got some data from a client
                        printf("hlfds: data (%d bytes) from %s on "
                            "socket %d\n", nbytes,
                            inet_ntop(remoteaddr.ss_family,
                                get_in_addr((struct sockaddr*)&remoteaddr),
                                remoteIP, INET6_ADDRSTRLEN),
                            i);
                        printf("recv'd: %s --\n", buf);
                        //if it starts with SENDALLCONTACTS, do that
                        if (strncmp(buf, "SENDALLCONTACTS", 15) == 0)
                        {
                            printf("*** TODO: got SENDALL\n");
                            sqlite3_stmt *stmt;
                            int rc = sqlite3_prepare(db, "select * from logentries", -1, &stmt, 0);
                            if(rc!=SQLITE_OK) 
                            {
                                 fprintf(stderr, "sql error #%d: %s\n", rc, sqlite3_errmsg(db));
                            } 
                            else while((rc = sqlite3_step(stmt)) != SQLITE_DONE) 
                            {
                                switch(rc) 
                                {
                                    case SQLITE_BUSY:
                                        fprintf(stderr, "busy, wait 1 seconds\n");
                                        sleep(1);
                                        break;
                                    case SQLITE_ERROR:
                                        fprintf(stderr, "step error: %s\n", sqlite3_errmsg(db));
                                        break;
                                    case SQLITE_ROW:
                                        {
                                            //int n = sqlite3_column_count(stmt);
                                            char tosend[256];
                                            sprintf(tosend, "CONTACT;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s#",
                                                sqlite3_column_text (stmt, 0),
                                                sqlite3_column_text (stmt, 1),
                                                sqlite3_column_text (stmt, 2),
                                                sqlite3_column_text (stmt, 3),
                                                sqlite3_column_text (stmt, 4),
                                                sqlite3_column_text (stmt, 5),
                                                sqlite3_column_text (stmt, 6),
                                                sqlite3_column_text (stmt, 7),
                                                sqlite3_column_text (stmt, 8),
                                                sqlite3_column_text (stmt, 9));
                                            printf("Sending to %s: %s\n", inet_ntop(remoteaddr.ss_family, get_in_addr((struct sockaddr*)&remoteaddr), remoteIP, INET6_ADDRSTRLEN), tosend);
                                            if (send(i, tosend, strlen(tosend), 0) == -1) {
                                             perror("send");
                                            }
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                            sqlite3_finalize(stmt);
                        }
                        //else if it starts with DELETE, do that
                        else if (strncmp(buf, "DELETE", 6) == 0)
                        {
                            char *token;
                            char *r = buf;
                            char *fieldtokens[10];
                            int fieldtokenindex = 0;
                            while ((token = strsep(&r, ";")) != NULL)
                            {
                                fieldtokens[fieldtokenindex] = token;
                                fieldtokenindex++;
                            }
                            if (fieldtokenindex == 2)
                            {
                                char tosend[128];
                                sprintf(tosend, "DELETE;%s", fieldtokens[1]);
                                printf("should delete: %s\n", fieldtokens[1]);
                                char sqlbuf[128];
                                sprintf(sqlbuf, "DELETE FROM logentries where luuid='%s'", fieldtokens[1]);
                                char *sqliteerr = sqlite3_malloc(2048);
                                const char *sql = sqlbuf;
                                int retval = sqlite3_exec(db, sql, 0, 0, &sqliteerr);
                                if (retval)
                                {
                                    printf("%s\n", sqliteerr);
                                }
                                else
                                {
                                    printf("Deleted log entry\n");
                                }

                                //tell others to delete
                                for(j = 0; j <= fdmax; j++) {
                                    // send to everyone!
                                    if (FD_ISSET(j, &master)) {
                                        // except the listener and ourselves
                                        if (j != listener && j != i) {
                                            if (send(j, tosend, strlen(tosend), 0) == -1) {
                                                perror("send");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //else if it has a # in it, split on # and parse each contact
                        else if (strrchr(buf, '#') != NULL)
                        {
                            char *token;
                            char *r = buf;
                            while ((token = strsep(&r, "#")) != NULL)
                            {
                                printf("line: %s\n", token);
                                char *fieldtoken;
                                char *fields[20];
                                int fieldtokenindex = 0;

                                while ((fieldtoken = strsep(&token, ";")) != NULL)
                                {
                                    fields[fieldtokenindex] = fieldtoken;
                                    fieldtokenindex++;
                                }

                                if (fieldtokenindex == 10)
                                {
                                    char sqlbuf[256];
                                    sprintf(sqlbuf, "REPLACE INTO logentries VALUES ('%s', %s, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')", 
                                        fields[0], fields[1], fields[2], fields[3], fields[4], 
                                        fields[5], fields[6], fields[7], fields[8], fields[9]);
                                    printf("sqlbuf: %s", sqlbuf);
                                    char *sqliteerr = sqlite3_malloc(2048);
                                    const char *sql = sqlbuf;
                                    int retval = sqlite3_exec(db, sql, 0, 0, &sqliteerr);
                                    if (retval)
                                    {
                                        printf("%s\n", sqliteerr);
                                    }
                                    else
                                    {
                                        printf("Inserted log entry\n");
                                    }

                                    //send to everyone
                                    char tosend[256];
                                    sprintf(tosend, "CONTACT;%s;%s;%s;%s;%s;%s;%s;%s;%s;%s#", 
                                        fields[0], fields[1], fields[2], fields[3], fields[4], 
                                        fields[5], fields[6], fields[7], fields[8], fields[9]);
                                    //should do per split contact
                                    for(j = 0; j <= fdmax; j++) {
                                        // send to everyone!
                                        if (FD_ISSET(j, &master)) {
                                            // except the listener and ourselves
                                            if (j != listener && j != i) {
                                                if (send(j, tosend, strlen(tosend), 0) == -1) {
                                                    perror("send");
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    printf("fieldtokenindex was not 10\n");
                                }
                            }
                        }
                        else
                        {
                            printf("*** made it here\n");
                        }

                        //wipe buf, we're done with it and we want null terminated strings
                        memset(buf, 0, sizeof(buf));
                    }
                } // END handle data from client
            } // END got new incoming connection
        } // END looping through file descriptors
    } // END for(;;)

    return 0;
}
