/*
** restart_service - runs restart argv[1], called from PHP, this should be setuid root
** Copyright (C) 2017  Nick Garner / N3WG, Pignology
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
      printf("Usage:\n%s <service>\n", argv[0]);
      exit(1);
    }

    char run[300];
    sprintf(run, "/sbin/restart %s", argv[1]);
    system(run);

    return 0;
}
