# README #

A guide on how to install this on a Raspberry Pi Zero W is here:  
[http://blog.pignology.net/2017/04/hamlog-field-day-server-with-twist.html](http://blog.pignology.net/2017/04/hamlog-field-day-server-with-twist.html)

### What is this repository for? ###

This repo contains the software needed to create a HamLog Field Day Server for use with the HamLog app for iOS, Android and Mac.



### Contribution Guidelines ###

Feel free to reach out to me or submit a pull request if you have suggestions or contributions.

### Contact ###

Nick / N3WG  
nwgarner@pignology.net
