#!/bin/bash
DATE=`date +%Y%m%d-%H%M`
echo

echo "Stopping services, if running"
systemctl stop hlfds
systemctl stop hlfds-announce
echo
echo

echo ">>>Validating dependencies."
echo ">>>apt-get -y install build-essential git-core apache2 php5 libapache2-mod-php5 php5-sqlite hostapd openssh-server libsqlite3-dev sqlite3"
apt-get -y install build-essential git-core apache2 php5 libapache2-mod-php5 php5-sqlite hostapd openssh-server libsqlite3-dev sqlite3
echo ">>>Restarting Apache."
systemctl daemon-reload 
service apache2 restart
echo
echo

echo ">>>Installing Web Pages"
echo ">>>cp -r www/* /var/www/html/"
cp -r www/* /var/www/html/
rm /var/www/html/index.html
echo
echo

echo ">>>Compiling HLFDS"
echo ">>>make -C src/"
make -C src/
echo
echo

echo ">>>Installing HLFDS"
echo ">>>make -C src/ install"
make -C src/ install
echo
echo

echo ">>>Installing Auto-Start Files"
cp -v etc/*.timer /lib/systemd/system/
cp -v etc/*.service /lib/systemd/system/
echo
echo

echo ">>>Enabling Auto-Start"
systemctl enable hlfds.timer
systemctl enable hlfds-announce.timer

echo "Starting services, will auto-start on reboot"
systemctl start hlfds
systemctl start hlfds-announce
echo

echo "Would you like to configure this host as a stand-alone access point?"
echo "NOTE: Files modified will be backed up, it will be a manual process to revert."
echo "NOTE: The system will reboot after the configuration"
echo -n "[Y/N]?: "
read ans
if [ "$ans" == "Y" ]; then
  echo "Configuring access point."
  cp -v /etc/hostapd/hostapd.conf /etc/hostapd/hostapd.conf.$DATE
  cp -v etc/hostapd.conf /etc/hostapd/
  cp -v /etc/network/interfaces /etc/network/interfaces.$DATE
  cp -v etc/interfaces /etc/network/interfaces

  echo ">>>Installing dnsmasq"
  apt-get -y install dnsmasq
  cp -v /etc/dnsmasq.conf /etc/dnsmasq.conf.$DATE
  cp -v etc/dnsmasq.conf /etc/
  service dnsmasq start

  echo ">>>Disabling udhcpcd"
  systemctl disable dhcpcd.service

  echo ">>>Enabling hostapd"
  systemctl enable hostapd.timer

  echo -n "Rebooting in "
  k=10
  while [ $k -gt 0 ]; do
    echo -n "$k ";
    sleep 1
    k=`expr $k - 1`
  done
  echo
  echo "Reboot"
  reboot
fi

echo "All set, point HamLog to this host."
echo

