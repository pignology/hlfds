<!DOCTYPE html>
<?php
date_default_timezone_set("UTC");
$date = date("r");

$db = new SQLite3('/opt/hlfds/hlfds.sqlite3');
$dbcount = $db->querySingle('SELECT count(*) FROM logentries');
//echo "count: " . $results . "<br>\n";


if (isset($_GET["restarthlfds"]))
{
  system("/opt/hlfds/bin/restart_service hlfds");
}


if (isset($_POST['rebootsubmit']))
{
  system("/opt/hlfds/bin/hlfds-reboot");
  echo "<meta http-equiv=\"refresh\" content=\"60;url=/\">";
  echo "<br>Will refresh in 60 seconds, please wait.<br>";

}

//check for shutdown
if (isset($_POST['shutdownsubmit']))
{
  system("/opt/hlfds/bin/hlfds-shutdown");
  echo "<br>Please wait 30 seconds before disconnecting the power.<br>";
}

?>

<html>
<head>
<title>HLFDS</title>
<LINK href="style.css" rel="stylesheet" type="text/css">
<?php if (count($_GET) > 0) echo "<meta http-equiv=\"refresh\" content=\"5;url=/\">"; ?>
<link rel="stylesheet" href="/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/datatables.min.css"/>
</head>
<body>
<div id="content">
<center>
<h1>Welcome to HLFDS!</h1>
<p> Current UTC Date/Time: <?php echo $date; ?> </p>
<br/>
</center>

<h2>Current Process Status</h2>
<table width="100%">
<thead><th>Service</th><th>Process Status</th><th>Operation</th></thead>
<tbody>

<tr>
  <td style="text-align:left;">HLFDS</td>
  <td style="text-align:left;"><?php system("ps wax | grep hlfds | grep -v grep | grep -v announce"); ?></td>
  <td><a href="/index.php?restarthlfds=1">Restart</a></td>
</tr>

</table>

<br>

<h2>Contacts in the database: <?php echo $dbcount; ?></h2><br>

<h2>Time / Uptime / Load</h2>
<?php system("uptime"); ?><br>


<br>


<h2>System Operations</h2>
<table width="100%">
<thead><th>Shutdown</th><th>Reboot</th></thead>
<tbody>
<tr>
<td width="50%" style="border-bottom: none;">
<!-- shutdown -->
This is permanent until you<br>unplug/plug-in the device.<br>Please wait 30 seconds before unplugging.<br>
<br>
</td>

<td width="50%" style="border-bottom: none;">
<!-- reboot -->
This happens immediately, no confirmation.<br>
</td>
</tr>

<tr>
<td width="50%" style="border-top: none;">
<form name="shutdown" action="/index.php" method="post">
<input type="submit" name="shutdownsubmit" value="Shutdown" style="height: 50px; width: 200px;"><br>
</form>
<br>
</td>

<td width="50%" style="border-top: none;">
<form name="reboot" action="/index.php" method="post">
<input type="submit" name="rebootsubmit" value="Reboot" style="height: 50px; width: 200px;"><br>
</form>
<br>
</td>
</table>

<br><br>


<p><em>Thank you for using HLFDS!<br/><br/>73,<br/>Nick<br/>N3WG<br/>Pignology</em></p>
<br/><br/>
<br/><br/>
</div>
<script type="text/javascript" src="/bootstrap.min.js"></script>
<script type="text/javascript" src="/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="/datatables.min.js"></script>

</body>
</html>

