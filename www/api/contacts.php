<?php
$ret = array();
$ret['data'] = array();
$db = new SQLite3('/opt/hlfds/hlfds.sqlite3');
$dbcount = $db->querySingle('SELECT count(*) FROM logentries');
$results = $db->query('SELECT * FROM logentries');
while ($row = $results->fetchArray()) {
    //var_dump($row);
  $entry = array();
  $epoch = $row['date'];
  $dt = new DateTime("@$epoch");  
  array_push($entry, $dt->format('Y-m-d H:i:s'));
  array_push($entry, $row['band']);
  array_push($entry, $row['mode']);
  array_push($entry, $row['callsign']);
  array_push($entry, $row['class']);
  array_push($entry, $row['section']);
  array_push($entry, $row['operator']);
  array_push($ret['data'], $entry);
}
echo json_encode($ret);
?>
