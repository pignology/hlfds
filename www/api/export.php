<?php
header("Content-type: text/csv");
header("Content-Disposition: attachment; filename=hlfds_contacts.csv");
header("Pragma: no-cache");
header("Expires: 0");

$ret = array();
$ret['data'] = array();
$db = new SQLite3('/opt/hlfds/hlfds.sqlite3');
$dbcount = $db->querySingle('SELECT count(*) FROM logentries');
$results = $db->query('SELECT * FROM logentries');
echo "Date,Band,Mode,Callsign,Class,Section,Operator\n";
while ($row = $results->fetchArray()) {
  $entry = array();
  $epoch = $row['date'];
  $dt = new DateTime("@$epoch");  

  echo $dt->format('Y-m-d H:i:s') . " UTC,";
  echo $row['band'] . ",";
  echo $row['mode'] . ",";
  echo $row['callsign'] . ",";
  echo $row['class'] . ",";
  echo $row['section'] . ",";
  echo $row['operator'] . "\n";
}
?>
